'use strict'

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify'),
    usemin = require('gulp-usemin'),
    rev = require('gulp-rev'),
    cleancss = require('gulp-clean-css'),
    flatmap = require('gulp-flatmap'),
    htmlmin = require('gulp-htmlmin');

gulp.task('sass',()=> {
    return gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
});

gulp.task('sass:watch',()=> {
    gulp.watch('./css/*.scss', gulp.series('sass'));
});

gulp.task('browser-sync',()=> {
    var files = ['./*.html', './css/*.css', './images/*.{png,jpg,gif,jpeg}', './JS/*.js'];
    browserSync.init(files, {
        server: {
            baseDir: "./"
        }
    });
});

gulp.task('clean',()=> {
    return del(['dist']);
});

gulp.task('copyfonts',()=> {
    return gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*')
        .pipe(gulp.dest('./dist/fonts'));
});

gulp.task('copyfonts2',()=> {
    return gulp.src('./fonts/*.{ttf,woff,eof,svg,eot,otf,woff2}*')
        .pipe(gulp.dest('./dist/fonts'));
});

gulp.task('imagemin',()=> {
    return gulp.src('./img/*.{png,jpg,jpeg,gif}')
        .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
        .pipe(gulp.dest('dist/img'));
});

gulp.task('usemin',()=> {
    return gulp.src('*.html')
        .pipe(flatmap((stream, file)=> {
            return stream
                .pipe(usemin({
                    css: [rev()],
                    html: [()=> { return htmlmin({ collapseWhitespace: true }) }],
                    js: [uglify(), rev()],
                    inlinejs: [uglify()],
                    inlinecss: [cleancss(), 'concat']
                }));
        }))
        .pipe(gulp.dest('dist/'));
});

gulp.task('default', gulp.parallel('browser-sync', 'sass:watch'));

gulp.task('build', gulp.series('clean', 'copyfonts','copyfonts2','imagemin','usemin'));

